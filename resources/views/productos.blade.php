@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ url('css/lib/easy-responsive-tabs.css') }}">
@endsection

@section('content')
    <div class="header">
        <div class="header_slide">
            <div class="header_bottom_left">
                @include('layouts.categorias')
            </div>
            <div class="header_bottom_right">
                @if (isset($rubro_actual))
                    <div class="row">
                        <div class="col-xs-6" style="float:left;">
                            Viendo Rubro: <strong>{{ ucfirst($rubro_actual) }}</strong>
                        </div>

                        <div class="col-xs-6" style="text-align: right;">
                            @if ($cantidad_productos >= 16)
                                <a href="{{ route('productos.por.rubro', ['rubro' => $rubro_actual, 'cantidad' => '16']) }}">16</a>
                                <a href="{{ route('productos.por.rubro', ['rubro' => $rubro_actual, 'cantidad' => '32']) }}">32</a>
                                <a href="{{ route('productos.por.rubro', ['rubro' => $rubro_actual, 'cantidad' => '56']) }}">56</a>
                                <a href="{{ route('productos.por.rubro', ['rubro' => $rubro_actual, 'cantidad' => '80']) }}">80</a>
                            @endif
                        </div>
                    </div>

                    @if ($cantidad_productos <= 0 && $ofertas)
                        <div class="section group">
                            <strong>
                                No hay ofertas para el rubro seleccionado
                            </strong>
                        </div>
                    @endif
                @endif
                <div class="slider">
                    <div class="clear">
                        @foreach ($productos_top_4 as $producto)
                            <div class="grid_1_of_4 images_1_of_4">
                                <a href="preview.html"><img src="{{ asset('images/sin-imagen.jpeg') }}" alt=""></a>
                                <h2>{!! $producto->getNombre() !!}</h2>
                                <div class="price-details">
                                    <div class="price-number">
                                        <p><span class="rupees">${{ $producto->getPrecio() }}</span></p>
                                    </div>
                                    <div class="add-cart">
                                        <h4><a href="#" class="add-to-cart" data-producto="{{ $producto->codigo }}">Agregar
                                                al carrito</a></h4>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="main">
        <div class="content col-xs-12">
            <div class="row">
                @foreach ($grupo_productos as $key => $fila_productos)
                    <div class="section group">
                        @foreach ($fila_productos as $producto)
                            <div class="grid_1_of_4 images_1_of_4">
                                <a href="preview.html"><img src="{{ asset('images/sin-imagen.jpeg') }}" alt=""></a>
                                <h2>{!! $producto->getNombre() !!}</h2>
                                <div class="price-details">
                                    <div class="price-number">
                                        <p><span class="rupees">${{ $producto->getPrecio() }}</span></p>
                                    </div>
                                    <div class="add-cart">
                                        <h4><a href="#" class="add-to-cart" data-producto="{{ $producto->codigo }}">Agregar
                                                al
                                                carrito</a></h4>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>

    <div class="content-pagenation">
        {!! $productos->appends(['busqueda' => $busqueda])->render() !!}
    </div>
@endsection