@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ url('css/lib/easy-responsive-tabs.css') }}">
@endsection

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/feature-pic1.jpg" alt=""/></a>
                    <h2>CUADERNOS A4 Colores Varios</h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$620.87</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Agregar al carrito</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/feature-pic2.jpg" alt=""/></a>
                    <h2>GLOBOS Bolsa x 20u. </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$899.75</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Agregar al carrito</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/feature-pic3.jpg" alt=""/></a>
                    <h2>Lapiceras Colores Varios </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$599.00</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Agregar al carrito</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/feature-pic4.jpg" alt=""/></a>
                    <h2>Cartulinas colores varios</h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$679.87</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Agregar al carrito</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="section group">
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/new-pic1.jpg" alt=""/></a>
                    <h2>Lorem Ipsum is simply </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$849.99</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Add to Cart</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/new-pic2.jpg" alt=""/></a>
                    <h2>Lorem Ipsum is simply </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$599.99</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Add to Cart</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/new-pic4.jpg" alt=""/></a>
                    <h2>Lorem Ipsum is simply </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$799.99</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Add to Cart</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="grid_1_of_4 images_1_of_4">
                    <a href="preview.html"><img src="images/new-pic3.jpg" alt=""/></a>
                    <h2>Lorem Ipsum is simply </h2>
                    <div class="price-details">
                        <div class="price-number">
                            <p><span class="rupees">$899.99</span></p>
                        </div>
                        <div class="add-cart">
                            <h4><a href="preview.html">Add to Cart</a></h4>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-pagenation">
        <li><a href="#"><</a></li>
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><span>....</span></li>
        <li><a href="#">></a></li>
    </div>
@endsection

@section('javascripts')
    <script src="{{ url('js/lib/easyResponsiveTabs.js') }}"></script>
    <script src="{{ url('js/lib/slides.min.jquery.js') }}"></script>
    <script>
        $(function () {
            $('#products').slides({
                preload: true,
                preloadImage: 'img/loading.gif',
                effect: 'slide, fade',
                crossfade: true,
                slideSpeed: 350,
                fadeSpeed: 500,
                generateNextPrev: true,
                generatePagination: false
            });
        });
    </script>
@endsection