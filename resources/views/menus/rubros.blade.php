<div class="categories">
    <ul>
        <h3>Rubros</h3>

        @foreach ($rubros as $rubro)
            <li>
                <a href="{{ route('productos.por.rubro', ['rubro' => $rubro->slug]) }}">
                    {{ $rubro->getNombre() }}
                </a>
            </li>
        @endforeach
    </ul>
</div>