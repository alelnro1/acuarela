@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/lib/slide-historia.css') }}" rel="stylesheet" type="text/css" media="all"/>
@endsection

@section('content')
    <div class="main">
        <div class="content">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="w3-content w3-display-container"> <img class="mySlides" src="{{ asset('images/001.jpg') }}" style="width:100%"> <img class="mySlides" src="images\002.jpg" style="width:100%"> <img class="mySlides" src="images\003.jpg" style="width:100%"> <img class="mySlides" src="images\004.jpg" style="width:100%">
                <button class="w3-button w3-black w3-display-left" onClick="plusDivs(-1)">&#10094;</button>
                <button class="w3-button w3-black w3-display-right" onClick="plusDivs(1)">&#10095;</button>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <script>
                var slideIndex = 1;
                showDivs(slideIndex);

                function plusDivs(n) {
                    showDivs(slideIndex += n);
                }

                function showDivs(n) {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    if (n > x.length) {slideIndex = 1}
                    if (n < 1) {slideIndex = x.length}
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    x[slideIndex-1].style.display = "block";
                }
            </script>
            <div class="section group">
                <div class="col_1_of_3 span_1_of_3">
                    <img src="{{ asset('images\nosotros-01.jpg') }}" alt="">
                    <h3>misión</h3>
                    <p>Nuestra misión es abastecer y  orientar a los diferentes públicos (comerciantes, instituciones, cliente  minorista), brindando atención personalizada, gran variedad y stock de  productos, y principalmente mantener el precio adecuado.</p>
                </div>

                <div class="col_1_of_3 span_1_of_3">
                    <img src="{{ asset('images\nosotros-02.jpg') }}" alt="">
                    <h3>historia</h3>
                    <p>Acuarela fue fundada en 1986. En  sus orígenes comenzó con el rubro papelera y librería, para luego con el paso  de los años y generaciones, comercializar los rubros cotillón y repostería. El  trabajo especializado de estos cuatro rubros, tanto en forma mayorista como  minorista, durante más de 30 años hizo que Acuarela sea un comercio líder,  elegido y valorado por el público que nos acompaña día a día.</p>
                </div>

                <div class="col_1_of_3 span_1_of_3">
                    <img src="{{ asset('images\nosotros-03.jpg') }}" alt="">
                    <h3>VISIÓN</h3>
                    <p>Nuestra visión es llegar a nuevos  clientes, brindándoles mediante nuevos medios de venta y comunicación, diferentes  herramientas de compra. Creemos que esto es posible por la relación que existe  entre nuestros productos y precios, y sobre todo por la trayectoria que nos  caracteriza. </p>
                </div>
            </div>
        </div>
    </div>
@endsection