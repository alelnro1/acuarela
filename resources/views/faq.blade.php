@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group"></div>
            <div class="faqs">
                <h2>&nbsp;</h2>
                <h2>&nbsp;</h2>
                <h2>&nbsp;</h2>
                <h2>Preguntas Frecuentes	</h2>
                <div id="posts">
                    <div class="tab bar">
                        <h4 class="post-title"> 1. ¿Cómo hago para realizar una compra Online? </h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Para poder realizar la compra online es necesario estar registrado en el sitio. Si aún no tenes una cuenta por hacerlo desde acá. Para registrarte vamos a solicitarte algunos datos personales, para que de esa forma podamos darte el mejor servicio posible.</p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 2. ¿Qué hago una vez que hice la compra Online? </h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Una vez que realices la compra, vas a poder elegir la forma de entrega y de pago, ya sea por envío o a retirar por el local y pago en efectivo al retirar o por trasnferencia bancaria. Cuando lo confirmes te va a llegar un mail con los datos necesarios para que puedas comunicarte con nosotros o acercarte al local.</p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 3. ¿Dónde puedo visitar el local? </h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Contamos con un local ubicado en José C. Paz en la calle Av. José Altube 1865 en el cual puede ver y comparar todos nuestros productos con la ayuda de nuestros vendedores.
                            ¡Visítenos!, lo asesoraremos con gusto.
                            También podés encontrarnos en Facebook, con el nombre "Distribuidora Acuarela" o ver todos nuestros productos en nuestra tienda online.
                        </p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 4. ¿Cuál es el horario de atención?</h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Pueden visitar el local de lunes a sábado en los horarios de 08:30hs a 12:00hs y de 15:30hs a 19:00hs.</p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 5. ¿Qué medios de pago aceptan?</h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Las compras pueden abonarse de diferentes formas dependiendo el lugar de compra.
                            En el local todas las compras pueden abonarse en efectivo y con tarjeta crédito o débito. La compras abonadas con Tarjeta de Crédito en 2 y 3 cuotas tiene un interés del 9%.
                            En caso de realizar la compra online puede solicitar el envío y abonarlo previamente por transferencia o pagarlo contra reembolso.
                        </p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 6. ¿Realizan envíos a domicilio?</h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Se realizan envíos a domicilio por pedidos que superen los $800.- de compra. </p>
                        <p>En caso de que el pedido sea superior a $6.000.- y el envío sea solicitado dentro de un radio de hasta 5km del local, el mismo será sin cargo. <br>
                            Y en caso de que el pedido sea superior a $9.000.- el envío sera sin cargo hasta un radio de 12km. del local.</p>
                        <p>En caso de que supere los 20km. el envío será coordinado y se hará cargo de abonarlo el comprador. </p>
                    </div>
                    <div class="tab bar">
                        <h4 class="post-title"> 7. ¿Hay descuento según el volumen de compra?</h4>
                    </div>
                    <div class="panel margin-lr-7">
                        <p>Si el pedido es superior a los $5000.- y se retira por la sucursal abonando en efectivo o con tarjeta de débito se aplica un descuento del 4%.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" src="{{ asset('js/lib/jquery.accordion.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#posts").accordion({
                header: "div.tab",
                alwaysOpen: false,
                autoheight: false
            });
        });
    </script>
@endsection