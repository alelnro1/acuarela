@extends('layouts.app')

@section('header-slide')
    @include('layouts.header-slide')
@endsection

@section('content')
    <div class="main">
        <div class="content">
            <div class="content_top">
                <div class="heading">
                    <h3>Productos Destacados!</h3>
                </div>
                <div class="clear"></div>
            </div>
            <div class="section group">
                @foreach ($destacados as $producto)
                    <div class="grid_1_of_4 images_1_of_4">
                        <a href="preview.html"><img src="{{ asset('images/sin-imagen.jpeg') }}" alt=""></a>
                        <h2>{!! $producto->getNombre() !!}</h2>
                        <div class="price-details">
                            <div class="price-number">
                                <p><span class="rupees">${{ $producto->getPrecio() }}</span></p>
                            </div>
                            <div class="add-cart">
                                <h4><a href="#" class="add-to-cart" data-producto="{{ $producto->codigo }}">Agregar
                                        al carrito</a></h4>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
