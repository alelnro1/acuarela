@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="col span_2_of_3">
                    <div class="contact-form">
                        <h2>Envianos tu Consulta</h2>
                        <form method="post" action="contact-post.html">
                            <div>
                                <span><label>Nombre Completo</label></span>
                                <span><input name="userName" type="text" class="textbox"></span>
                            </div>
                            <div>
                                <span><label>E-mail</label></span>
                                <span><input name="userEmail" type="text" class="textbox"></span>
                            </div>
                            <div>
                                <span><label>Teléfono</label></span>
                                <span><input name="userPhone" type="text" class="textbox"></span>
                            </div>
                            <div>
                                <span><label>Mensaje</label></span>
                                <span><textarea name="userMsg"> </textarea></span>
                            </div>
                            <div>
                                <span><input type="submit" value="Enviar" class="myButton"></span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col span_1_of_3">
                    <div class="contact_info">
                        <h3>Encontranos acá!</h3>
                        <div class="map">
                            <iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0"
                                    marginwidth="0"
                                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6574.448953483613!2d-58.75491228343789!3d-34.522540320565966!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x72d4ab1a8d405fda!2sDistribuidora+Papelera+%22Acuarela+%22!5e0!3m2!1ses-419!2sar!4v1507047960689"></iframe>
                            <br>
                            <small>
                                <a href="https://www.google.com.ar/maps/place/Distribuidora+Papelera+%22Acuarela+%22/@-34.522241,-58.751174,15z/data=!4m5!3m4!1s0x0:0x72d4ab1a8d405fda!8m2!3d-34.522241!4d-58.751174"
                                   target="_blank" style="color:#666;text-align:left;font-size:12px">Ver Mapa</a>
                            </small>
                        </div>
                    </div>
                    <div class="company_address">
                        <h3>InformaciÓn de Contacto :</h3>
                        <p>Av. José Altube 1865,</p>
                        <p>José C. Paz, Buenos Aires,</p>
                        <p>Argentina</p>
                        <p>Telefono:(+54) 02320 42-2331</p>
                        <p>Email: <span><a href=mailto:ventas@distacuarela.com.ar> ventas@distacuarela.com.ar</a></span>
                        </p>
                        <p>Seguinos en: <span><a href="https://www.facebook.com/distribuidoraacuarela" target="_blank">Facebook</a> </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection