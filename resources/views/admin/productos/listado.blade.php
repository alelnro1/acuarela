@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('css/styles.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.0/css/buttons.bootstrap4.min.css">
@endsection


@section('content')
    <br><br>
        <table border="1" width="100%" id="productos">
            <thead>
            <tr>
                <th>Producto</th>
                <th>Es Destacado?</th>
                <th>Es Oferta?</th>
                <th>Es Slide?</th>
            </tr>
            </thead>

            <tbody></tbody>
        </table>
@endsection

@section('javascripts')
    <script type="text/javascript" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var columns = [
                {
                    name: "Producto",
                    data: "Producto",
                    sortable: false
                },
                {
                    data: "Destacado",
                    orderable: false
                },
                {
                    data: "Oferta",
                    orderable: false
                },

                {
                    data: "Slide",
                    orderable: false
                }
            ];

            tabla = $('#productos').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/admin/productos/cargar',
                    type: 'POST',
                    dataSrc: "records"
                },
                language:

                    {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                columns: columns
            });

            $('body')
                .on('click', '.destacar, .eliminar-destacado', function (e) {
                    e.preventDefault();

                    var elem = $(this);

                    $.ajax({
                        url: $(this).data('href'),
                        data: {
                            destacado: elem.data('destacado')
                        },
                        type: 'POST',
                        success: function (data) {
                            elem.parent().empty().append(data);
                        }, error: function (data) {

                        }
                    });
                })
                .on('click', '.ofertar, .eliminar-oferta', function (e) {
                    e.preventDefault();

                    var elem = $(this);

                    $.ajax({
                        url: $(this).data('href'),
                        data: {
                            oferta: elem.data('oferta')
                        },
                        type: 'POST',
                        success: function (data) {
                            elem.parent().empty().append(data);
                        }, error: function (data) {

                        }
                    });
                })
                .on('click', '.agregar-slide, .eliminar-slide', function (e) {
                    e.preventDefault();

                    var elem = $(this);

                    $.ajax({
                        url: $(this).data('href'),
                        data: {
                            slide: elem.data('slide')
                        },
                        type: 'POST',
                        success: function (data) {
                            elem.parent().empty().append(data);
                        }, error: function (data) {

                        }
                    });
                });
        });
    </script>
@endsection
