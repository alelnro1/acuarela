@if (!$destacado)
    <input type="checkbox"
           class="destacar"
           data-href="{{ route('admin.productos.destacar', ['producto' => $producto->id]) }}"
           data-destacado="1"/>
    Destacar
@else
    <a href="" data-href="{{ route('admin.productos.eliminar-destacado', ['producto' => $producto]) }}"
        class="eliminar-destacado"
        data-destacado="0">
        Eliminar destacado
    </a>
@endif
