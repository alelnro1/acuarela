@if (!$slide)
    <input type="checkbox"
           class="agregar-slide"
           data-href="{{ route('admin.productos.agregar-slide', ['producto' => $producto->id]) }}"
           data-slide="1"/>
    Poner en slide
@else
    <a href="" data-href="{{ route('admin.productos.eliminar-slide', ['producto' => $producto]) }}"
       class="eliminar-slide"
       data-slide="0">
        Eliminar de slide
    </a>
@endif
