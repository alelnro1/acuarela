@if (!$oferta)
    <input type="checkbox"
           class="ofertar"
           data-href="{{ route('admin.productos.ofertar', ['producto' => $producto->id]) }}"
           data-oferta="1"/>
    Ofertar
@else
    <a href="" data-href="{{ route('admin.productos.eliminar-oferta', ['producto' => $producto]) }}"
       class="eliminar-oferta"
       data-oferta="0">
        Eliminar oferta
    </a>
@endif
