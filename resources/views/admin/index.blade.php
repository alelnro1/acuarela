@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="row">
                <br>
                <div class="col-xs-12">
                    <div class="help-block">
                        <span>
                            Seleccione el archivo que contiene los productos a ser importados.
                            Recuerde que en el proceso de importación, el sitio entrará en modo de
                            mantenimiento durante unos instantes. Se recomienda realizar dicho
                            proceso en horarios no laboral
                        </span>
                    </div>

                    <br><br><br>

                    @if (session('resultado'))
                        <div><strong>{{ session('resultado') }}</strong></div>
                    @endif

                    <fieldset class="form-group">
                        <legend>Importar Productos</legend>

                        <form action="{{ route('importar.productos') }}" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}

                            <input type="file" name="productos">

                            <input type="submit">
                        </form>
                    </fieldset>

                    <br><br><br>

                    <fieldset>
                        <legend>Importar Imágenes</legend>

                        <form action="" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <input type="file" name="imagenes">
                        </form>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
@endsection