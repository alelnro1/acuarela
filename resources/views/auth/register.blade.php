@extends('layouts.app')

@section('content')
    <div class="main">
        <section class="register">
            <div class="register-full">
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="register-left">
                    <div class="register-in">
                        <h1>Registrate acá para poder comprar!</h1>
                        <p>Ingresando tus datos en nuestro sitio, vas a poder realizar las compras de una manera mucho mas facil y rapida desde la comodidad de tu casa. Una vez que ingreses los datos y hagas las compra podras seleccionar si te lo enviamos o lo venis a retirar. En caso que quieras que lo enviemos ya vamos a tener tus datos para poder comunicarnos con vos!</p>
                    </div>
                </div>
                <div class="register-right">
                    <div class="register-in">
                        <h2>Ingresa tus datos acá</h2>
                        <div class="register-form">
                            <form action="{{ route('register') }}" method="post">
                                {{ csrf_field() }}

                                <div class="fields-grid">
                                    <h4>Datos Personales</h4>

                                    <div class="styled-input agile-styled-input-top">
                                        <input type="text" name="nombre" value="{{ old('nombre') }}" required>
                                        <label>Nombre</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="apellido" value="{{ old('apellido') }}" required>
                                        <label>Apellido</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="dni" value="{{ old('dni') }}" required>
                                        <label>DNI/CUIL/CUIT</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="tel" name="telefono" value="{{ old('telefono') }}" required>
                                        <label>Teléfono</label>
                                        <span></span>
                                    </div>

                                    <h4>Dirección</h4>
                                    <div class="styled-input">
                                        <input type="text" name="calle" value="{{ old('calle') }}" required>
                                        <label> Calle</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="ciudad" value="{{ old('ciudad') }}" required>
                                        <label> Ciudad</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="cp" value="{{ old('cp') }}" required>
                                        <label> Código Postal</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="provincia" value="{{ old('provincia') }}" required>
                                        <label> Provincia</label>
                                        <span></span>
                                    </div>

                                    <h4>Datos de Usuario</h4>
                                    <div class="styled-input">
                                        <input type="email" name="email" value="{{ old('email') }}" required autocomplete="off">
                                        <label>E-Mail</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input name="password" type="password" required>

                                        <label> Contraseña</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="password" name="password_confirmation" required>
                                        <label> Confirmar Contraseña</label>
                                        <span></span>
                                    </div>
                                    <div class="clear"> </div>

                                </div>
                                <input type="submit" value="Registrarme">
                            </form>
                        </div>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

        </section>
    </div>
    {{--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
