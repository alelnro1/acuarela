@extends('layouts.app')

@section('content')
    <div class="main_login">
        <div class="content_login">
            <div class="section group">
                <div class="resp-tabs-container">
                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                        <div class="got">
                            <h6>¿Estás registrado? Ingresa tus datos y logueate.</h6>
                        </div>
                        <div class="register">
                            @if ($errors->has('email'))
                                <div class="alert alert-success">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif

                            <form action="{{ route('login') }}" method="post">
                                {{ csrf_field() }}
                                <div class="fields-grid">
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <div class="styled-input agile-styled-input-top">
                                        <input type="email" name="email" required>
                                        <label>E-Mail</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="password" name="password" required>
                                        <label>Contraseña</label>
                                        <span></span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <input type="checkbox" id="brand" value="">
                                <label for="brand"><span></span> Recordame?</label>
                                <p></p>
                                <div class="login-bottom">
                                    <ul>
                                        <li>
                                            <input type="submit" value="Iniciar Sesión"/>
                                        </li>
                                        <li>
                                            <a href="#">¿Olvidaste la contraseña?</a>
                                        </li>
                                    </ul>
                                </div>
                            </form>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
