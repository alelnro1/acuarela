@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="cart1">
                    <div class="cart-top">
                        <div class="cart-experience">
                            <h4>Mi lista de productos</h4>
                        </div>
                        <div class="cart-login">
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="cart-bottom">
                        <div class="table">
                            <table style="width:100%">
                                <tbody>
                                <tr class="main-heading">
                                    {{--<th>Imágenes</th>--}}
                                    <th class="long-txt">Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                                @foreach ($productos as $producto)
                                    <tr class="cake-top">
                                        {{--}}<td class="cakes">
                                            <div class="product-img">
                                                <img src="images/feature-pic2.jpg">
                                                {{ dump(print_r(Cart::get($producto->rowId), true)) }}
                                            </div>
                                        </td>--}}
                                        <td class="cake-text">
                                            <div class="product-text">
                                                <h3>{{ Cart::get($producto->rowId)->name }}</h3>
                                            </div>
                                        </td>
                                        <td class="quantity">
                                            <div class="product-right">
                                                <input min="1" type="number" id="quantity" name="quantity"
                                                       value="{{ Cart::get($producto->rowId)->qty }}"
                                                       class="form-control input-small prev-cant-prod">
                                            </div>
                                        </td>
                                        <td class="price" style="text-align: right;">
                                            <h4>
                                                $
                                                <span id="prev-precio-prod">
                                                    {{ number_format(Cart::get($producto->rowId)->price, 2) }}
                                                </span>
                                            </h4>
                                        </td>
                                        <td class="top-remove" style="text-align: right;">
                                            <h4>
                                                $ {{
                                                  number_format(
                                                    Cart::get($producto->rowId)->qty * Cart::get($producto->rowId)->price
                                                    , 2)
                                                }}
                                            </h4>
                                        </td>
                                        <td class="top-remove">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="#" style="font-size: 0.75em;
                                                color: #FFFFFF;
                                                cursor: pointer;
                                                background: #34BBB0;
                                                padding: 0.5em 1em;
                                                border-radius: 3px;">
                                                Eliminar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="vocher">

                            <div class="dis-total">
                                <h1>Total</h1>
                                <div class="tot-btn">
                                    <a class="shop" href="{{ route('home') }}">
                                        Continuar comprando
                                    </a>
                                    <a class="check" href="#">Finalizar pedido</a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection