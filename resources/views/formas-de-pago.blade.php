@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="images/formasdepago-01.jpg" alt="" />
                    <h3>Pago en el local </h3>
                    <p>En el local se puede abonar con efectivo y con tarjeta de débito y crédito en 1 pago sin interés. En caso que se abone con tarjeta de crédito en 2 o 3 pagos, se aplicará un interés del 9%.</p>
                </div>
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="images/formasdepago-02.jpg" alt="" />
                    <h3>DePÓSITO O TRANSFERENCIA BANCARIA </h3>
                    <p>En caso que la compra se realice de forma online, se puede solicitar que se envíe el pedido una vez abonado el mismo por tranferencia o depósito bancario.
                    </p>
                </div>
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="images/formasdepago-03.jpg" alt="" />
                    <h3>CONTRAREEMBOLSO</h3>
                    <p>Si la compra supera los $800.- lo podemos enviar y se la puede abonar junto con el envío por contrareembolso.</p>
                </div>
            </div>
            <h3> Si el pedido es superior a los $5000.- y se retira por la sucursal abonando en efectivo o con tarjeta de débito se aplica un descuento del 4%.</h3>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" src="{{ asset('js/lib/jquery.accordion.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#posts").accordion({
                header: "div.tab",
                alwaysOpen: false,
                autoheight: false
            });
        });
    </script>
@endsection