@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <div class="section group">
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="{{ asset('images/enviosydevoluciones-01.jpg') }}" alt="" />
                    <h3>Envios a domicilio  </h3>
                    <p>La compra tiene que superar un monto mínimo de $800.-
                    </p>
                    <p>Clientes con repartos ya asignados, se coordina con el comercio.</p>
                    <p>Se entrega sin cargo si:</p>
                    <p> -El pedido es superior a $6000.- dentro de un radio de 5km de distancia del local.</p>
                    <p>-El pedido es superior a $9000.- dentro de un radio de 12km de distancia del local.</p>
                </div>
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="{{ asset('images/enviosydevoluciones-02.jpg') }}" alt="" />
                    <h3>retirÁ POR NUESTRO LOCAL </h3>
                    <p>Una vez que realices la compra online, podes elegir si querés retirarlo por el local o que te lo envíen y acercarte a nuestro negocio ubicado en Av. José Altube 1865,
                        José C. Paz, donde lo tendremos listo para cuando vengas. Te recomendamos comunicarte previamente para coordinar el retiro.
                    </p>
                </div>
                <div class="grid_1_of_3 images_1_of_3">
                    <img src="{{ asset('images/enviosydevoluciones-03.jpg') }}" alt="" />
                    <h3>DEVOLUCIONES</h3>
                    <p>?????????????????????????????</p>
                </div>
            </div>
        </div>
    </div>
@endsection