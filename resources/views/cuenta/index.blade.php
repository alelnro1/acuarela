@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <section class="register">
                <div class="register-full">
                    <div class="register-left">
                        <div class="register-in-Left">
                            <div class="vertical-menu">
                                <!-- /////////// Begin Dropdown //////////// -->
                                <div class='swanky_wrapper'>
                                    <input id='menu' name='radio' type='radio'>
                                    <label for='menu'>
                                        <span>Menú</span>
                                        <div class='lil_arrow'></div>
                                        <div class='bar'></div>
                                        <div class='swanky_wrapper__content'>
                                            @include('cuenta.menu')
                                        </div>
                                    </label>
                                </div>
                                <!-- /////////// End Dropdown //////////// -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="register-right">
            <div class="register-in">
                <h2>Historial de compras</h2>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <ul class="product-list-vertical">
                    <li>

                        <a href="#" class="product-photo">
                            <img src="images/feature-pic4.jpg" height="80"/>
                        </a>

                        <div class="product-details">

                            <h2><a>Cartulina colores varios</a></h2>

                            <div class="product-rating">
                            </div>
                            <button>Comprar de nuevo</button>
                            <p class="product-price">$599.00</p>
                        </div>
                    </li>
                </ul>

                <ul class="product-list-vertical">
                    <li>

                        <a href="#" class="product-photo">
                            <img src="images/feature-pic4.jpg" height="80"/>
                        </a>

                        <div class="product-details">

                            <h2><a>Cartulina colores varios</a></h2>

                            <div class="product-rating">
                                <div></div>
                            </div>
                            <button>Comprar de nuevo</button>
                            <p class="product-price">$599.00</p>
                        </div>
                    </li>
                </ul>

                <ul class="product-list-vertical">
                    <li>

                        <a href="#" class="product-photo">
                            <img src="images/feature-pic4.jpg" height="80"/>
                        </a>

                        <div class="product-details">

                            <h2><a>Cartulina colores varios</a></h2>

                            <div class="product-rating">
                                <div></div>
                            </div>
                            <button>Comprar de nuevo</button>
                            <p class="product-price">$599.00</p>
                        </div>
                    </li>
                </ul>
                {{--<div class="content-pagenation">
                    <li><a href="#"><</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><span>....</span></li>
                    <li><a href="#">></a></li>
                </div>--}}
            </div>
        </div>
        <div class="clear"></div>
        </section>
    </div>
@endsection

