@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <section class="register">
                <div class="register-full">
                    <div class="register-left">
                        <div class="register-in-Left">
                            <div class="vertical-menu">
                                <!-- /////////// Begin Dropdown //////////// -->
                                <div class='swanky_wrapper'>
                                    <input id='menu' name='radio' type='radio'>
                                    <label for='menu'>
                                        <span>Menú</span>
                                        <div class='lil_arrow'></div>
                                        <div class='bar'></div>
                                        <div class='swanky_wrapper__content'>
                                            <ul>
                                                @include('cuenta.menu')
                                            </ul>
                                        </div>
                                    </label>
                                </div>
                                <!-- /////////// End Dropdown //////////// -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="register-right">
                    <div class="register-in">
                        <h2>Mis Datos</h2>
                        <div class="register-form">
                            <form action="#" method="post">
                                <div class="fields-grid">
                                    <div class="styled-input agile-styled-input-top">
                                        <input type="text" name="First name" required>
                                        <label>Nombre</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="Last name" required>
                                        <label>Apellido</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="dni" required>
                                        <label>DNI/CUIL/CUIT</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="email" name="email" required>
                                        <label>E-Mail</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="tel" name="Phone" required>
                                        <label>Teléfono</label>
                                        <span></span>
                                    </div>
                                    <h4>Dirección</h4>
                                    <div class="styled-input">
                                        <input type="text" name="calle" required>
                                        <label> Calle</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="ciudad" required>
                                        <label> Ciudad</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="cp" required>
                                        <label> Código Postal</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="provincia" required>
                                        <label> Provincia</label>
                                        <span></span>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                                <input type="submit" value="Modificar Datos">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clear"> </div>
            </section>
        </div>
        <div class="clear"> </div>
    </div>
@endsection