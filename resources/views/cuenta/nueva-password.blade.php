@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="content">
            <section class="register">
                <div class="register-full">
                    <div class="register-left">
                        <div class="register-in-Left">
                            <div class="vertical-menu">
                                <!-- /////////// Begin Dropdown //////////// -->
                                <div class='swanky_wrapper'>
                                    <input id='menu' name='radio' type='radio'>
                                    <label for='menu'>
                                        <span>Menú</span>
                                        <div class='lil_arrow'></div>
                                        <div class='bar'></div>
                                        <div class='swanky_wrapper__content'>
                                            @include('cuenta.menu')
                                        </div>
                                    </label>
                                </div>
                                <!-- /////////// End Dropdown //////////// -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="register-right">
                    <div class="register-in">
                        <h2>Modificá tu contraseña acá</h2>
                        <div class="register-form">
                            <form action="#" method="post">
                                <div class="fields-grid">
                                    <div class="styled-input agile-styled-input-top">
                                        <input type="text" name="email" required>
                                        <label>Email</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="pass" required>
                                        <label>Nueva Contraseña</label>
                                        <span></span>
                                    </div>
                                    <div class="styled-input">
                                        <input type="text" name="pass" required>
                                        <label>Repetir Contraseña</label>
                                        <span></span>
                                    </div>
                                    <div class="clear"> </div>
                                </div>
                                <input type="submit" value="Cambiar contraseña">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clear"> </div>
            </section>
        </div>
        <div class="clear"> </div>
    </div>
@endsection