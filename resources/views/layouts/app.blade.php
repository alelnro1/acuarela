<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Distribuidora Acuarela</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{ asset('css/lib/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ asset('css/lib/slider.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    @yield('styles')

    <script type="text/javascript" src="{{ asset('js/lib/jquery-1.7.2.min.js') }}"></script>
</head>
<body>
<div class="wrap">
    <div class="header">
        <div class="headertop_desc">
            <div class="call">
                <p><span>Necesitas ayuda?</span> Mandanos un mail a <span
                            class="number"> ventas@distacuarela.com.ar</span></p>
            </div>
            <div class="account_desc">
                @if (Auth::user() && Auth::user()->esAdmin())
                    <span>Logueado como Admin: <strong>{{ Auth::user()->getNombre() }}</strong></span>

                    <ul>
                        <li>
                            <a href="{{ route('admin.productos') }}">Productos</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.importacion') }}">Importar</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" class="btn"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-sign-out"></i> Cerrar Sesión
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                        </li>
                    </ul>
                @elseif (Auth::user())
                    <span>Hola <strong>{{ Auth::user()->getNombre() }}</strong></span>

                    <ul>
                        <li><a href="{{ route('mi-cuenta.index') }}">Mi Cuenta</a></li>
                        <li>
                            <a href="{{ route('logout') }}" class="btn"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-sign-out"></i> SALIR
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                        </li>
                    </ul>
                @else
                    <ul>
                        <li><a href="{{ route('register') }}">Registrate</a></li>
                        <li><a href="{{ route('login') }}">Iniciá Sesión</a></li>
                    </ul>
                @endif
            </div>
            <div class="clear"></div>
        </div>
        <div class="header_top">
            <div class="logo">
                <a href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt=""/></a>
            </div>
            <div class="cart">
                <p>Bienvenidos a nuestra tienda Online! <br><span>Carrito:</span>
                <div id="dd" class="wrapper-dropdown-2"><span id="carrito-cant-productos"></span> objeto(s) - $ <span
                            id="carrito-precio"></span>
                    <ul class="dropdown" id="carrito"
                        data-carrito-agregar-url="{{ route('carrito.agregar') }}"
                        data-carrito-cargar-url="{{ route('carrito.cargar') }}">
                        <li>No tenes objetos en tu carrito de compras.</li>
                    </ul>

                </div>
                <p></p>
                <br><br>
                <div style="float:right; display: none;">
                    {{--<a href="{{ route('carrito.previsualizar') }}" class="btn btn-success" id="previsualizar-compra">
                        Previsualizar Compra
                    </a>--}}
                </div>
            </div>

            <script type="text/javascript">
                function DropDown(el) {
                    this.dd = el;
                    this.initEvents();
                }

                DropDown.prototype = {
                    initEvents: function () {
                        var obj = this;

                        obj.dd.on('click', function (event) {
                            $(this).toggleClass('active');
                            event.stopPropagation();
                        });
                    }
                }

                $(function () {

                    var dd = new DropDown($('#dd'));

                    $(document).click(function () {
                        // all dropdowns
                        $('.wrapper-dropdown-2').removeClass('active');
                    });

                });

            </script>

            <div class="search_box">
                <form method="POST"
                      @if (!isset($ofertas))
                      action="{{ route('productos.buscar', ['rubro' => Request::segment(2)]) }}"
                      @else
                      action="{{ route('ofertas.buscar', ['rubro' => Request::segment(2)]) }}"
                        @endif
                >
                    {{ csrf_field() }}
                    <input type="text" name="busqueda" placeholder="Ingrese el producto a buscar"
                           @if (isset($busqueda)) value="{{ $busqueda }}" @endif>
                    <input type="hidden" name="rubro" value="{{ Request::segment(2) }}">
                    <input type="submit" value="">
                </form>
            </div>


            <div class="clear"></div>
        </div>
        <div class="header_bottom">
            <div class="menu">
                <ul>
                    <li @if (Request::is('home')) class="active" @endif>
                        <a href="{{ route('home') }}">Productos</a>
                    </li>

                    <li @if (Request::is('ofertas')) class="active" @endif>
                        <a href="{{ route('ofertas') }}">¡Ofertas!</a>
                    </li>

                    <li @if (Request::is('envios')) class="active" @endif>
                        <a href="{{ route('envios') }}">Envíos y Devoluciones</a>
                    </li>

                    <li @if (Request::is('formas-de-pago')) class="active" @endif>
                        <a href="{{ route('formas-de-pago') }}">Formas de Pago</a>
                    </li>

                    <li @if (Request::is('historia')) class="active" @endif>
                        <a href="{{ route('historia') }}">Nuestra historia</a>
                    </li>

                    <li @if (Request::is('preguntas-frecuentes')) class="active" @endif>
                        <a href="{{ route('preguntas-frecuentes') }}">Preguntas Frecuentes</a>
                    </li>

                    <li @if (Request::is('contacto')) class="active" @endif>
                        <a href="{{ route('contacto.index') }}">Contacto</a>
                    </li>

                    <div class="clear"></div>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
        @yield('header-slide')
    </div>
    @yield('content')
</div>
<div class="footer">
    <div class="wrap">
        <div class="section group">
            <div class="col_1_of_4 span_1_of_4">
                <h4>InformaciÓn</h4>
                <ul>
                    <li><a href="nosotros.html">Quiénes Somos</a></li>
                    <li><a href="contacto.html">Dónde estamos</a></li>
                    <li><a href="Envios.html">Formas de pago</a></li>
                    <li><a href="contacto.html">Contacto</a></li>
                </ul>
            </div>
            <div class="col_1_of_4 span_1_of_4">
                <h4>PorquÉ elegirnos</h4>
                <ul>
                    <li><a href="nosotros.html">Sobre Nosotros</a></li>
                    <li><a href="faqs.html">FAQs</a></li>
                    <li><a href="envios.html">Envíos y Devoluciones</a></li>
                </ul>
            </div>
            <div class="col_1_of_4 span_1_of_4">
                <h4>Mi Cuenta</h4>
                <ul>
                    <li><a href="iniciasesion.html">Inicia Sesión</a></li>
                    <li><a href="index.html">Mi Carrito</a></li>
                    <li><a href="contacto.html">Ayuda</a></li>
                </ul>
            </div>
            <div class="col_1_of_4 span_1_of_4">
                <h4>Contacto</h4>
                <ul>
                    <li><span>Av. José Altube 1865,
José C. Paz</span></li>
                    <li><span>02320 42-2331</span></li>
                    <li><span>ventas@distacuarela.com.ar</span></li>


                </ul>
                <div class="social-icons">
                    <h4>Seguinos!</h4>
                    <ul>
                        <li><a href="https://www.facebook.com/distribuidoraacuarela" target="_blank"><img
                                        src="{{ asset('images/facebook.png') }}" alt=""/></a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copy_right">
        <p><br>
            Distribuidora Acuarela © Derechos Reservados | Diseñado por <a target="_blank"
                                                                           href="http://www.pixl.com.ar">PIXL</a></p>
    </div>
</div>

<div id="dialog-agregar-al-carrito" title="Indique la cantidad a comprar" style="display: none;">
    <p class="validateTips">Indique cuántos items desea agregar al carrito.</p>

    <form>
        <input type="text" name="cantidad" id="cantidad-agregar" autocomplete="off">
        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </form>
</div>

<a href="#" id="toTop"><span id="toTopHover"> </span></a>

<script type="text/javascript" src="{{ asset('js/lib/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/lib/startstop-slider.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>

<script src="{{ asset('js/cart.js') }}"></script>

@yield('javascripts')
</body>
</html>

