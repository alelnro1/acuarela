<div class="categories">
    <ul>
        <h3>Categorias</h3>
        @if ($ofertas)
            @foreach ($rubros as $rubro)
                <li>
                    <a href="{{ route('ofertas.por.rubro', ['rubro' => $rubro->getSlug()]) }}">
                        {{ $rubro->nombre }}
                    </a>
                </li>
            @endforeach
        @else
            @foreach ($rubros as $rubro)
                <li>
                    <a href="{{ route('productos.por.rubro', ['rubro' => $rubro->getSlug()]) }}">
                        {{ $rubro->nombre }}
                    </a>
                </li>
            @endforeach
        @endif
    </ul>
</div>