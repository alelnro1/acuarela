<div class="header_slide">
    <div class="header_bottom_left">
        @include('layouts.categorias')
    </div>
    @if (!$ofertas)
        <div class="header_bottom_right">
            <div class="slider">
                <div id="slider">
                    <div id="mover">
                        @foreach ($productos_slider as $producto)
                            <div id="slide-1" class="slide">
                                <div class="slider-img">
                                    <a href="{{ route('producto') }}"><img src="{{ asset('images/sin-imagen.jpeg') }}"
                                                                           alt="learn more"/></a>
                                </div>
                                <div class="slider-text">
                                    <h1>Nueva<br>
                                        <span>Oferta</span></h1>
                                    <h2>{{ $producto->getNombre() }}</h2>
                                    <a href="#" class="button add-to-cart" data-producto="{{ $producto->codigo }}">
                                        Comprar ahora
                                    </a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    @endif
    <div class="clear"></div>
</div>