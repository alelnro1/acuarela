var Carrito = {
    carrito_listado : $('ul#carrito'),
    url_agregar_carrito: $('ul#carrito').data('carrito-agregar-url'),
    url_cargar_carrito: $('ul#carrito').data('carrito-cargar-url'),
    cant_productos: $('span#carrito-cant-productos'),
    precio_total: $('span#carrito-precio'),

    Mostrar: function (data) {
        Carrito.cant_productos.html(data.cant_productos);
        Carrito.precio_total.html(data.precio_total);
        Carrito.carrito_listado.empty().append(data.carrito);

        if (data.cant_productos > 0) {
            $('#previsualizar-compra').parent().show();
        }
    },

    AgregarProducto: function (producto_id, cantidad) {
        $.ajax({
            url: Carrito.url_agregar_carrito,
            type: 'POST',
            data: {
                'producto': producto_id,
                'cantidad': cantidad
            },
            dataType: 'json',
            success: function (data) {
                Carrito.Mostrar(data);
            }
        })
    },

    Cargar: function () {
        $.ajax({
            url: Carrito.url_cargar_carrito,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                Carrito.Mostrar(data);
            }
        })
    }
};

$(document).ready(function () {
    Carrito.Cargar();

    $('body')
        .on('click', '.add-to-cart', function (e) {
            e.preventDefault();

            var producto_id = $(this).data('producto');

            $( "#dialog-agregar-al-carrito" ).dialog({
                width: 350,
                modal: true,
                buttons: {
                    Agregar: function() {
                        var cantidad = $('input#cantidad-agregar').val();

                        Carrito.AgregarProducto(producto_id, cantidad);

                        $(this).dialog( "close" );
                    }
                },
                close: function() {
                    $('input#cantidad-agregar').val('');
                }
            });
        })
        .on('change input keyup', '.prev-cant-prod', function () {
            var elem = $(this);

            prev-precio-prod
        });
});