<?php

namespace App;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model implements Buyable
{
    protected $table = "productos";

    protected $fillable = [
        'codigo',
        'descripcion',
        'precio',
        'rubro_id',
        'imagen',
        'slug'
    ];

    public function Rubro()
    {
        return $this->belongsTo(Rubro::class, 'rubro_id', 'interfaz_id');
    }

    public function getNombre()
    {
        return $this->descripcion;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function esDestacado()
    {
        return $this->es_destacado;
    }

    public function esOferta()
    {
        return $this->es_oferta;
    }

    public function esSlide()
    {
        return $this->es_slide;
    }

    public function getProductos($rubro_id = null, $cantidad_por_pagina = 16, $busqueda = null, $ofertas = false)
    {
        $productos = $this;

        if ($rubro_id !== null) {
            $productos = $productos->where('rubro_id', $rubro_id);
        }

        if ($busqueda != null) {
            $productos = $productos->where('descripcion', 'LIKE', '%' . $busqueda . '%');
        }

        // Se buscó sin escribir nada y sin un rubro, traigo 500 productos
        /*if (!isset($productos) && !$ofertas) {
            $productos = Producto::take(100);
        }*/

        if ($ofertas) {
            $productos = $productos->where('es_oferta', true);
        }

        $productos = $productos->where('descripcion', '<>', '')->orderBy('descripcion');

        $productos = $productos->paginate($cantidad_por_pagina);

        return $productos;
    }

    /**
     * Obtenemos todas las ofertas, sin discriminar por rubro
     *
     * @return mixed
     */
    public static function getOfertasSinRubro()
    {
        $ofertas = self::where('es_oferta', true)->get();

        return $ofertas;
    }

    /**
     * Listado de productos para ver desde el panel del admin
     *
     * @param $start
     * @param $length
     * @return mixed
     */
    public static function getProductosAdmin($start = 0, $length = 10)
    {
        $productos =
            self::offset($start)
                ->limit($length)
                ->where('descripcion', '<>', '')
                ->orderBy('es_destacado', 'DESC')
                ->orderBy('es_oferta', 'DESC')
                ->orderBy('es_slide', 'DESC')
                ->orderBy('descripcion', 'ASC')
                ->get();

        return $productos;
    }

    /**
     * Desde el Admin, traemos el listado de productos filtrados, limitados por la cantidad $length
     *
     * @param $start
     * @param $length
     * @param $filtro
     * @return mixed
     */
    public static function getProductosAdminFiltrados($start, $length, $filtro)
    {
        $comitentes =
            self::where('descripcion', 'LIKE', '%' . $filtro . '%')
                ->where('descripcion', '<>', '')
                ->offset($start)
                ->limit($length)
                ->orderBy('descripcion')
                ->orderBy('es_destacado', 'DESC')
                ->orderBy('es_oferta', 'DESC')
                ->orderBy('es_slide', 'DESC')
                ->get();

        return $comitentes;
    }

    /**
     * Desde el Admin, traemos la cantidad de productos que entran en el filtro
     *
     * @param $filtro
     * @return mixed
     */
    public static function getCantProductosAdminFiltrados($filtro)
    {
        $comitentes =
            self::where('descripcion', 'LIKE', '%' . $filtro . '%')
                ->where('descripcion', '<>', '')
                ->count();

        return $comitentes;
    }

    /**
     * Traemos el total de productos del sistema
     *
     * @return int
     */
    public static function getTotalProductos()
    {
        $cant_productos = self::where('descripcion', '<>', '')->count();

        return $cant_productos;
    }

    /**
     * Desde el Admin, destacamos o quitamos el destacado de un producto
     * @return void
     */
    public function destacar($es_destacado = true)
    {
        $this->es_destacado = $es_destacado;
        $this->save();
    }

    /**
     * Desde el Admin, destacamos o quitamos la oferta de un producto
     * @return void
     */
    public function ofertar($es_oferta = true)
    {
        $this->es_oferta = $es_oferta;
        $this->save();
    }

    public function slide($es_slide = true)
    {
        $this->es_slide = $es_slide;
        $this->save();
    }

    /**
     * Obtenemos los productos destacados
     *
     * @return mixed
     */
    public static function getDestacados()
    {
        $destacados =
            self::where('es_destacado', true)
                ->where('descripcion', '<>', '')
                ->orderBy('descripcion')
                ->get();

        return $destacados;
    }

    /**
     * Obtenemos los productos para poner en el slide
     *
     * @return mixed
     */
    public static function getProductosParaSlide()
    {
        $productos =
            self::where('es_slide', true)
                ->where('descripcion', '<>', '')
                ->orderBy('descripcion')
                ->get();

        return $productos;
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->descripcion;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->precio;
    }
}
