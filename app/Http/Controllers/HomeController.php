<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Rubro;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rubros = Rubro::getRubrosConSubrubros();
        $destacados = Producto::getDestacados();
        $slider = Producto::getProductosParaSlide();

        return view('home', [
            'ofertas' => false,
            'rubros' => $rubros,
            'productos_slider' => $slider,
            'destacados' => $destacados
        ]);
    }
}
