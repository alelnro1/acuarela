<?php

namespace App\Http\Controllers;

use App\Producto;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarritoController extends Controller
{
    /**
     * Agregamos un producto al carrito y devolvemos el carrito actualizado, con el precio total y la cantidad de prods
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agregar(Request $request)
    {
        $producto_codigo = $request->producto;
        $cantidad = $request->cantidad;

        $producto = Producto::where('codigo', $producto_codigo)->first();

        Cart::add($producto_codigo, $producto->getNombre(), $cantidad, $producto->getPrecio());

        if (Auth::id()) {
            Cart::restore(Auth::id());
            Cart::store(Auth::id());
        }

        return $this->cargarContenidoCarritoResumen();
    }

    /**
     * Devolvemos el resumen del carrito para mostrar en todas las vistas
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cargarContenidoCarritoResumen()
    {
        $cart = view('carrito.listado', ['carrito' => Cart::content()])->render();
        $cart_precio = Cart::subtotal();
        $cart_cant_productos = Cart::count();

        return response()->json([
            'carrito' => $cart,
            'precio_total' => $cart_precio,
            'cant_productos' => $cart_cant_productos
        ], 200);
    }

    public function previsualizar()
    {
        $productos = Cart::content();

        return view('carrito.previsualizar', [
            'productos' => $productos
        ]);
    }
}
