<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontSimpleController extends Controller
{
    public function envios()
    {
        return view('envios');
    }

    public function historia()
    {
        return view('nuestra-historia');
    }

    public function faq()
    {
        return view('faq');
    }

    public function formasDePago()
    {
        return view('formas-de-pago');
    }
}
