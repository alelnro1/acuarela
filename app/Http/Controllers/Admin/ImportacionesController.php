<?php

namespace App\Http\Controllers\Admin;

use App\Producto;
use App\Rubro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportacionesController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function importarProductos(Request $request)
    {
        $request->validate([
            'productos' => 'required|mimes:txt'
        ]);

        $file = fopen($request->productos, "r");

        $rubros_existentes = Rubro::all()->groupBy('interfaz_id');

        Artisan::call('down');

        DB::unprepared("TRUNCATE TABLE productos");

        while (!feof($file)) {
            $line = fgets($file);

            $linea_producto = explode(";", $line);

            if ($this->lineaEsValida($line, $rubros_existentes)) {
                $codigo = ltrim(trim(mb_convert_encoding($linea_producto[0], 'UTF-8', 'ASCII')));
                $descripcion = ltrim(trim(mb_convert_encoding(htmlentities($linea_producto[1]), 'UTF-8', 'ASCII')));
                $rubro_id = $linea_producto[3];
                $imagen = ltrim(trim(mb_convert_encoding($linea_producto[4], 'UTF-8', 'ASCII')));
                $precio = ltrim(trim(mb_convert_encoding($linea_producto[5], 'UTF-8', 'ASCII')));
                $slug = str_slug($descripcion);

                Producto::create([
                    'codigo' => $codigo,
                    'descripcion' => $descripcion,
                    'precio' => str_replace(',', '', $precio),
                    'rubro_id' => $rubro_id,
                    'imagen' => $imagen,
                    'slug' => $slug
                ]);

                Log::info("Memory: " . memory_get_peak_usage() / 1024 / 1024);
            }
        }

        Artisan::call('up');

        return redirect(route('importacion'))->with([
            'resultado' => 'Importación de productos exitosa'
        ]);
    }


    /**
     * Verifico que una linea del TXT tenga todos los campos para poder grabarla en la base
     * Si no los tiene, se saltea
     *
     * @param $linea
     */
    private function lineaEsValida($linea, $rubros_existentes)
    {
        $linea = explode(";", $linea);

        // Verifico que existan
        if (
            isset($linea[0]) &&
            isset($linea[1]) &&
            isset($linea[3]) &&
            isset($linea[4])
        ) {
            if ($rubros_existentes->get($linea[3])) {
                return true;
            }
        }

        return false;
    }
}
