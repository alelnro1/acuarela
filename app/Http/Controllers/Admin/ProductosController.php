<?php

namespace App\Http\Controllers\Admin;

use App\Producto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ProductosController extends Controller
{
    public function index()
    {
        return view('admin.productos.listado');
    }

    /**
     * Traemos los productos utilizando DataTables ServerSide, con filtro y cantidad de productos a listar,
     * dinámico, por AJAX.
     *
     * @param Request $request
     * @return string
     */
    public function cargarProductos(Request $request)
    {
        // Obtengo los datos
        $draw = $request->draw;
        $start = $request->start;
        $length = $request->length;
        $filtro = $request->search['value'];
        $orden_columna_index = $request->order[0]['column'];
        $order_by = $request->columns[$orden_columna_index]['name'];
        $order_type = $request->order[0]['dir'];

        $productos = Producto::getProductosAdmin($start, $length);
        $total_productos = Producto::getTotalProductos();

        if ($filtro) {
            $productos = Producto::getProductosAdminFiltrados($start, $length, $filtro);

            $total_productos_filtrados = Producto::getCantProductosAdminFiltrados($filtro);;
        } else {
            $total_productos_filtrados = $total_productos;
        }

        $productos_return = [];

        foreach ($productos as $producto) {
            $producto_aux = [];

            $producto_aux['Producto'] = $producto->getNombre();

            $producto_aux['Destacado'] =
                view('admin.productos.input-destacado', ['producto' => $producto, 'destacado' => $producto->esDestacado()])
                    ->render();

            $producto_aux['Oferta'] =
                view('admin.productos.input-ofertar', ['producto' => $producto, 'oferta' => $producto->esOferta()])
                    ->render();

            $producto_aux['Slide'] =
                view('admin.productos.input-slide', ['producto' => $producto, 'slide' => $producto->esSlide()])
                    ->render();

            array_push($productos_return, $producto_aux);
        }

        $return = [];

        $return['draw'] = $draw;
        $return['records'] = $productos_return;
        $return['recordsTotal'] = $total_productos;
        $return['recordsFiltered'] = $total_productos_filtrados;

        return json_encode($return);
    }

    /**
     * Desde el Admin destacamos o quitamos el destacado de un producto para que aparezca en el Home
     *
     * @param Producto $producto
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destacarProducto(Producto $producto, Request $request)
    {
        $destacado = $request->destacado;

        $producto->destacar($destacado);
        $producto->save();

        return response()->json(
            view('admin.productos.input-destacado', ['producto' => $producto, 'destacado' => $producto->esDestacado()])
                ->render()
        );
    }

    /**
     * Desde el Admin destacamos o quitamos el destacado de un producto para que aparezca en el Home
     *
     * @param Producto $producto
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ofertarProducto(Producto $producto, Request $request)
    {
        $oferta = $request->oferta;

        $producto->ofertar($oferta);
        $producto->save();

        return response()->json(
            view('admin.productos.input-ofertar', ['producto' => $producto, 'oferta' => $producto->esDestacado()])
                ->render()
        );
    }

    public function productoASlide(Producto $producto, Request $request)
    {
        $slide = $request->slide;

        $producto->slide($slide);
        $producto->save();

        return response()->json(
            view('admin.productos.input-slide', ['producto' => $producto, 'slide' => $producto->esSlide()])
                ->render()
        );
    }
}
