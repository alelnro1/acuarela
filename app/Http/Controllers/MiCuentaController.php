<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiCuentaController extends Controller
{
    public function index()
    {
        return view('cuenta.index');
    }

    public function datos()
    {
        return view('cuenta.datos');
    }

    public function cambiarPassword()
    {
        return view('cuenta.nueva-password');
    }
}
