<?php

namespace App\Http\Controllers;

use App\Rubro;
use App\Subrubro;
use Illuminate\Http\Request;

class RubrosController extends Controller
{
    public function productosPorRubro(Rubro $rubro)
    {
        $rubros = Rubro::all();

        $productos = $rubro->getProductos();

        $top_4 = $productos->take(4);

        $productos = $productos->paginate(20);

        return view('productos', [
            'rubros' => $rubros,
            'productos_top_4' => $top_4,
            'productos' => $productos->slice(5)
        ]);
    }
}
