<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Rubro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class ProductosController extends Controller
{
    /**
     * @var boolean Esto indica si estamos buscando por ofertas
     */
    protected $ofertas = false;

    public function ver()
    {
        return view('producto');
    }

    public function buscar(Request $request)
    {
        $rubro = $request->rubro;

        $busqueda = $request->busqueda;

        $productos = new Producto();

        // Si estamos dentro de una categoria, filtramos esa categoria
        if ($rubro != null) {
            $rubro_id = Rubro::where('nombre', 'LIKE', '%'. $rubro . '%')->first()->getId();

            $productos = $productos->getProductos($rubro_id,16, $busqueda, $this->ofertas);
        } else {
            $productos = $productos->getProductos(null, 16, $busqueda, $this->ofertas);
        }

        $rubros = Rubro::all();
        $productos_listado = $productos->items();
        $cantidad_productos = $productos->count();

        $top_4 = $productos->take(4);

        $grupos_productos = $this->armarGruposParaListado(array_slice($productos_listado, 4));

        return view('productos', [
            'ofertas' => $this->ofertas,
            'rubros' => $rubros,
            'rubro_actual' => $rubro,
            'busqueda' => $busqueda,
            'productos_top_4' => $top_4,
            'grupo_productos' => $grupos_productos,
            'productos' => $productos,
            'cantidad_productos' => $cantidad_productos,
            'cantidad_por_pagina' => 16
        ]);
    }

    /**
     * @param Rubro $rubro
     * @param int $cantidad_por_pagina Tiene que ser SI O SI multiplo de 4
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listarPorRubro(Rubro $rubro, $cantidad_por_pagina = 16)
    {
        $busqueda = Request::capture()->request->get('busqueda');

        // Si la cantidad no es divisible por 4 => Mostramos por defecto 16
        if ($cantidad_por_pagina % 4 != 0) $cantidad_por_pagina = 16;

        $rubros = Rubro::all();
        $productos = new Producto();

        $productos = $productos->getProductos($rubro->getId(), $cantidad_por_pagina, $busqueda, $this->ofertas);
        $productos_listado = $productos->items();
        $cantidad_productos = $productos->count();

        $top_4 = $productos->take(4);

        $grupos_productos = $this->armarGruposParaListado(array_slice($productos_listado, 4));

        return view('productos', [
            'ofertas' => $this->ofertas,
            'rubro_actual' => $rubro->getNombre(),
            'rubros' => $rubros,
            'productos_top_4' => $top_4,
            'productos' => $productos,
            'cantidad_productos' => $cantidad_productos,
            'busqueda' => $busqueda,
            'grupo_productos' => $grupos_productos,
            'cantidad_por_pagina' => $cantidad_por_pagina
        ]);
    }

    /**
     * Devolvemos un array de productos para listar
     * [
     *  [producto_1, producto_2, producto_3, producto_4],
     *  [producto_5, producto_6, producto_7, producto_8]
     *  [producto_9, producto_10, producto_11, producto_12]
     * ]
     *
     * @param $productos
     * @return array
     */
    private function armarGruposParaListado($productos)
    {
        $cant_por_grupo = 4;

        $grupo = [];
        $cant_productos = count($productos);
        $clave_producto = 0;

        while ($clave_producto < $cant_productos) {
            $subgrupo = [];
            for ($i = 1; $i <= $cant_por_grupo; $i++) {
                $subgrupo[] = $productos[$clave_producto];
                $clave_producto++;
            }

            $grupo[] = $subgrupo;
        }

        return $grupo;
    }
}