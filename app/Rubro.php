<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    protected $table = "rubros";

    protected $primaryKey = "interfaz_id";

    protected $fillable = [
        'nombre',
        'slug'
    ];

    public function Productos()
    {
        return $this->hasMany(Producto::class, 'rubro_id', 'interfaz_id');
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getId()
    {
        return $this->interfaz_id;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function getRubrosConSubrubros()
    {
        $rubros = Rubro::orderBy('nombre')->get();

        return $rubros;
    }

    public function getProductos()
    {
        $productos = $this->load('Productos')->first();

        return $productos->Productos;
    }
}
