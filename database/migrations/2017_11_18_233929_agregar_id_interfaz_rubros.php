<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarIdInterfazRubros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign(['rubro_id']);
        });

        Schema::table('rubros', function (Blueprint $table) {
            $table->dropColumn(['id']);
        });

        Schema::table('rubros', function (Blueprint $table) {
            $table->integer('interfaz_id')->unsigned();
        });

        Schema::table('rubros', function (Blueprint $table) {
            $table->index('interfaz_id');
        });

        Schema::table('rubros', function (Blueprint $table) {
            $table->primary('interfaz_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
