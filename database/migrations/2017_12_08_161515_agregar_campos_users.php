<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarCamposUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nombre');
            $table->string('apellido');
            $table->string('dni');
            $table->string('telefono');
            $table->string('calle');
            $table->string('ciudad');
            $table->string('cp');
            $table->string('provincia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'name',
                'dni',
                'telefono',
                'calle',
                'ciudad',
                'cp',
                'provincia'
            ]);
        });
    }
}
