<?php

Auth::routes();

Route::get('contacto', 'HomeController@contact')->name('contacto');
Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('importar', 'ProductosController@importar');

Route::group(['prefix' => 'productos'], function () {
    Route::get('/{rubro}/{cantidad?}', 'ProductosController@listarPorRubro')->name('productos.por.rubro');

    Route::post('{rubro?}', 'ProductosController@buscar')->name('productos.buscar');
});

Route::group(['prefix' => 'ofertas'], function () {
    Route::get('/', 'OfertasController@index')->name('ofertas');
    Route::get('{rubro}/{cantidad?}', 'OfertasController@listarPorRubro')->name('ofertas.por.rubro');
    Route::post('{rubro?}', 'OfertasController@buscar')->name('ofertas.buscar');
});

Route::get('envios', 'FrontSimpleController@envios')->name('envios');
Route::get('historia', 'FrontSimpleController@historia')->name('historia');
Route::get('preguntas-frecuentes', 'FrontSimpleController@faq')->name('preguntas-frecuentes');
Route::get('formas-de-pago', 'FrontSimpleController@formasDePago')->name('formas-de-pago');

Route::get('contacto', 'ContactoController@index')->name('contacto.index');

// Esta va a ser la visualizacion de un producto
Route::get('producto', 'ProductosController@ver')->name('producto');


Route::group(['prefix' => 'carrito'], function () {
    Route::post('agregar', 'CarritoController@agregar')->name('carrito.agregar');
    Route::get('cargar', 'CarritoController@cargarContenidoCarritoResumen')->name('carrito.cargar');

    Route::get('previsualizar', 'CarritoController@previsualizar')->name('carrito.previsualizar');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('mi-cuenta', 'MiCuentaController@index')->name('mi-cuenta.index');
    Route::get('mi-cuenta/datos', 'MiCuentaController@datos')->name('mi-cuenta.datos');
    Route::get('mi-cuenta/historial', 'MiCuentaController@historial')->name('mi-cuenta.historial-compras');
    Route::get('mi-cuenta/cambiar-password', 'MiCuentaController@cambiarPassword')->name('mi-cuenta.cambiar-password');
});

include_once('admin.web.php');
