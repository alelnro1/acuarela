<?php

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {

    Route::group(['prefix' => 'productos'], function () {
        Route::get('/', 'ProductosController@index')->name('admin.productos');
        Route::post('cargar', 'ProductosController@cargarProductos');

        /* Destacar Producto */
        Route::post('{producto}/destacar', 'ProductosController@destacarProducto')->name('admin.productos.destacar');
        Route::post('{producto}/eliminar-destacado', 'ProductosController@destacarProducto')->name('admin.productos.eliminar-destacado');

        /* Ofertar producto */
        Route::post('{producto}/ofertar', 'ProductosController@ofertarProducto')->name('admin.productos.ofertar');
        Route::post('{producto}/eliminar-oferta', 'ProductosController@ofertarProducto')->name('admin.productos.eliminar-oferta');

        /* Producto en slide */
        Route::post('{producto}/agregar-slide', 'ProductosController@productoASlide')->name('admin.productos.agregar-slide');
        Route::post('{producto}/eliminar-slide', 'ProductosController@productoASlide')->name('admin.productos.eliminar-slide');
    });

    Route::get('importar', 'ImportacionesController@index')->name('admin.importacion');
    Route::post('importar-productos', 'ImportacionesController@importarProductos')->name('importar.productos');
});